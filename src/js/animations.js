import app from './createApp';

/*Анимации*/

function hammerEntryAnimation(hammer) {
  setTimeout(() => {
    hammer.visible = true;
    hammerAnimation();
  }, 2000);

  function hammerAnimation() {
    if (hammer.y <= 258) hammer.y += 1;
    else return;
    requestAnimationFrame(hammerAnimation);
  }
}

function mainButtonAnimation(button) {
  app.ticker.add(() => {
    button.count += 0.02;
    button.scale.x = 1 + Math.sin(button.count) / 10;
    button.scale.y = 1 + Math.sin(button.count) / 10;
  });
}

export { hammerEntryAnimation, mainButtonAnimation };
