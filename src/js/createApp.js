import * as PIXI from 'pixi.js';

const pageContainer = document.querySelector('.page-container');

/*Функция создает сцену*/
function createApp() {
  const newApp = new PIXI.Application({
    width: 1390,
    height: 640
  });

  //Вставлям сцену в html
  pageContainer.append(newApp.view);
  return newApp;
}

const app = createApp();

export default app;
