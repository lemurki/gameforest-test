import * as PIXI from 'pixi.js';

const loader = PIXI.Loader.shared;
const resources = PIXI.Loader.shared.resources;
const backgroundContainer = new PIXI.Container();
const menuContainer = new PIXI.Container();
const frontContainer = new PIXI.Container();
const mainElementsContainer = new PIXI.Container();
const graphics = new PIXI.Graphics();

export {
  loader,
  resources,
  backgroundContainer,
  menuContainer,
  frontContainer,
  mainElementsContainer,
  graphics
};
