import { loader } from './helpers';
import setup from './setupImages';

/*Функция загружает изображения и запускает функцию их установки*/

function loadingImages() {
  loader.add('assets/images/atlas.json').load(setup);
}
export default loadingImages;
