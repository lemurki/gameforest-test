import { menuContainer } from './helpers';

/*Функция устанавливает обработчики событий*/

function handlers(
  { hammer, choosed, okButton, final, graphics, button, logo },
  state,
  stairsArray,
  previewBackArray
) {
  /*Устанавливаем обработчики*/

  hammer.on('pointerdown', hammerOnClick);

  previewBackArray.forEach((previewBack) => {
    previewBack.on('pointerdown', previewBackOnClick);
  });

  okButton.on('pointerdown', oKButtonOnClick);

  logo.on('pointerdown', () => {
    window.location.href = 'https://game-forest.com/';
  });

  button.on('pointerdown', () => {
    window.location.href = 'https://game-forest.com/';
  });

  /*Функции обработчики*/

  function hammerOnClick() {
    hammer.visible = false;
    menuContainer.visible = true;
  }

  function previewBackOnClick(еvent) {
    const choosedPreview = еvent.currentTarget;

    choosed.x = choosedPreview.x + 10;
    choosed.y = choosedPreview.y + 5;
    choosed.visible = true;
    okButton.x = choosedPreview.x - 5;
    okButton.visible = true;

    if (
      state.targetStair !== choosedPreview.id &&
      state.targetStair !== state.previousStair
    ) {
      stairsArray[state.currentStair].visible = false;
      state.previousStair = state.targetStair;
      state.targetStair = choosedPreview.id;
      stairsArray[state.previousStair].visible = false;
      stairsArray[state.previousStair].y = -33;
      stairsArray[state.targetStair].visible = true;

      stairAnimation();
    }
  }

  function stairAnimation() {
    if (stairsArray[state.targetStair].y != -13) {
      stairsArray[state.targetStair].y += 1;
    } else return;
    requestAnimationFrame(stairAnimation);
  }

  function oKButtonOnClick() {
    state.currentStair = state.targetStair;
    menuContainer.visible = false;

    setTimeout(() => {
      graphics.visible = true;
      final.visible = true;

      finalAnimation(final);
    }, 300);
  }

  function finalAnimation() {
    if (final.scale.x <= 1) {
      final.scale.x += 0.05;
      final.scale.y += 0.05;
    } else return;
    requestAnimationFrame(finalAnimation);
  }
}

export default handlers;
