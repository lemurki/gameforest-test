import * as PIXI from 'pixi.js';
import app from './createApp';
import {
  backgroundContainer,
  menuContainer,
  frontContainer,
  mainElementsContainer
} from './helpers';
import { resources, graphics } from './helpers';
import handlers from './handlers';
import { hammerEntryAnimation, mainButtonAnimation } from './animations';

/*Функция создает и устанавливает спрайты изображений*/

function setup() {
  const back = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['back.jpg']
  );

  const dec2 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['dec2.png']
  );
  const austin = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['austin.png']
  );
  austin.x = 695;
  austin.y = 107;

  const oldStair = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['oldStair.png']
  );
  oldStair.x = 833;
  oldStair.y = 54;

  const newStair1 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['newStair1.png']
  );

  newStair1.x = 898;
  newStair1.y = -33;
  newStair1.speed = 3;
  newStair1.visible = false;

  const newStair2 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['newStair2.png']
  );

  newStair2.x = 898;
  newStair2.y = -33;
  newStair2.speed = 3;
  newStair2.visible = false;

  const newStair3 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['newStair3.png']
  );

  newStair3.x = 898;
  newStair3.y = -33;
  newStair3.speed = 3;
  newStair3.visible = false;

  const dec1 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['dec1.png']
  );

  dec1.x = 1122;
  dec1.y = 438;

  const hammer = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['hammer.png']
  );

  hammer.x = 1087;
  hammer.y = 228;
  hammer.speed = 4;
  hammer.visible = false;
  hammer.interactive = true;
  hammer.buttonMode = true;

  const previewBackTexture =
    resources['assets/images/atlas.json'].textures['previewBack.png'];

  const choosed = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['choosed.png']
  );

  choosed.x = 988;
  choosed.y = 16;
  choosed.visible = false;

  const preview1 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['preview1.png']
  );

  preview1.x = 879;
  preview1.y = -28;

  const preview2 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['preview2.png']
  );

  preview2.x = 1010;
  preview2.y = -65;

  const preview3 = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['preview3.png']
  );

  preview3.x = 1134;
  preview3.y = 10;

  const okButton = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['okButton.png']
  );

  okButton.x = 969;
  okButton.y = 121;
  okButton.interactive = true;
  okButton.buttonMode = true;
  okButton.visible = false;

  const logo = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['logo.png']
  );

  logo.x = 32;
  logo.y = 5;
  logo.interactive = true;
  logo.buttonMode = true;

  const button = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['button.png']
  );

  button.x = 502 / 0.7;
  button.y = 499 / 0.9;
  button.anchor.x = 0.5;
  button.anchor.y = 0.5;
  button.interactive = true;
  button.buttonMode = true;
  button.count = 0;

  const final = new PIXI.Sprite(
    resources['assets/images/atlas.json'].textures['final.png']
  );

  final.x = 391 / 0.54;
  final.y = 53 / 0.2;
  final.anchor.x = 0.5;
  final.anchor.y = 0.5;
  final.scale.set(0, 0);
  final.visible = false;

  graphics.beginFill(0x000000, 0.6);
  graphics.drawRect(0, 0, 1390, 640);
  graphics.endFill();
  graphics.visible = false;

  /*Формируем контейнеры для спрайтов и загружает в них нужные спрайты*/

  //Вставляем в сцену контейнер для изображений на фоне
  app.stage.addChild(backgroundContainer);
  //Вставляем в сцену контейнер для изображений на переднем плане
  app.stage.addChild(frontContainer);
  //Вставляем в сцену контейнер для блока меню
  app.stage.addChild(menuContainer);
  //Вставляем в сцену контейнер для главных элементов
  app.stage.addChild(mainElementsContainer);

  /*Наполняем контейнер для фоновых изображений*/

  backgroundContainer.addChild(back);
  backgroundContainer.addChild(dec2);
  backgroundContainer.addChild(austin);

  /*Наполняем контейнер для изображений на переднем плане*/

  frontContainer.addChild(oldStair);
  frontContainer.addChild(newStair1);
  frontContainer.addChild(newStair2);
  frontContainer.addChild(newStair3);
  frontContainer.addChild(dec1);
  frontContainer.addChild(hammer);
  hammerEntryAnimation(hammer);

  /*Наполняем контейнер для элементов меню*/
  const previewBackArray = [];

  for (let i = 0; i <= 2; i++) {
    const previewBack = new PIXI.Sprite(previewBackTexture);
    previewBack.x = 851 + i * 128;
    previewBack.y = 11;
    previewBack.id = i;
    previewBack.interactive = true;
    previewBack.buttonMode = true;
    menuContainer.addChild(previewBack);
    previewBackArray.push(previewBack);
  }
  menuContainer.addChild(choosed);
  menuContainer.addChild(preview1);
  menuContainer.addChild(preview2);
  menuContainer.addChild(preview3);
  menuContainer.addChild(okButton);
  menuContainer.visible = false;

  /*Наполняем контейнер для основных элементов*/

  mainElementsContainer.addChild(graphics);
  mainElementsContainer.addChild(logo);
  mainElementsContainer.addChild(button);
  mainElementsContainer.addChild(final);
  mainButtonAnimation(button);

  const sprites = {
    back,
    dec2,
    austin,
    oldStair,
    newStair1,
    newStair2,
    newStair3,
    hammer,
    previewBackTexture,
    choosed,
    preview1,
    preview2,
    preview3,
    okButton,
    logo,
    button,
    final,
    graphics
  };

  const state = {
    currentStair: 3,
    targetStair: 3,
    previousStair: null
  };

  /*Массив лестниц*/
  const stairsArray = [newStair1, newStair2, newStair3, oldStair];

  /*Подключаем обработчики*/
  handlers(sprites, state, stairsArray, previewBackArray);
}
export default setup;
