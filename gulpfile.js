const gulp = require('gulp');
const postcss = require('gulp-postcss');
const buffer = require('vinyl-buffer');
const source = require('vinyl-source-stream');
// `PostCSS` plugins
const postcssPresetEnv = require('postcss-preset-env');
const sourcemaps = require('gulp-sourcemaps');
const postcssimport = require('postcss-import');
const postcssNesting = require('postcss-nesting');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
// js
const browserify = require('browserify');
const uglify = require('gulp-uglify');
// gzip
const gzip = require('gulp-gzip');

/**
 * CSS function
 */

const plugins = [
  // Concat all into one file
  postcssimport(),
  // CSS nesting
  postcssNesting(),
  // Transform future css syntax
  postcssPresetEnv({
    stage: 2
  }),
  // Add prefixes
  autoprefixer(),
  // Minify css
  cssnano({
    preset: 'default'
  })
];

const css = () => {
  return gulp
    .src('./src/css/styles.css')
    .pipe(sourcemaps.init())
    .pipe(postcss(plugins))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(gzip({ append: true }))
    .pipe(gulp.dest('./dist/css/'));
};
exports.css = css;

/**
 * JS function
 */

const js = () => {
  return browserify('src/js/index.js')
    .transform('babelify', { presets: ['env'], plugins: ['transform-runtime'] }) // transform modules
    .bundle() // bundling
    .pipe(source('bundle.js'))
    .pipe(buffer()) // prepare to minifying
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify()) // minify
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/js/'))
    .pipe(gzip({ append: true }))
    .pipe(gulp.dest('./dist/js/'));
};
exports.js = js;

// Function to build css and js
const b = () => gulp.parallel(css, js);
exports.b = b;

// Function to watch css and js
const w = () => {
  gulp.watch(['./src/css/**/*.css'], gulp.parallel('css'));
  gulp.watch(['./src/js/**/*.js'], gulp.parallel('js'));
};
exports.w = w;
